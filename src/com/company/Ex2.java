package com.company;

public class Ex2 implements Runnable{
    String smth;
    Ex2(String smth)
    {
        this.smth = smth;
    }
    public void run() {
        try {
            for(int i=0; i<10; i++)
            {
                System.out.println(smth + " - " + i);
                Thread.sleep(6000);
            }
        }catch (Exception e){
            System.out.println("Error");
        }
    }

    public static void main(String[] args)
    {
        Thread t1 = new Thread(new Ex2("SE-Thread1"));
        Thread t2 = new Thread(new Ex2("SE-Thread2"));
        Thread t3 = new Thread(new Ex2("SE-Thread1"));
        t1.start();
        t2.start();
        t3.start();
    }
}
